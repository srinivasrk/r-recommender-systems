# README #

Recommender systems are machine learning techniques to provide recommendation based on exisiting data
In this project we will understand the working of "RecommenderLab" and perform an User Based Collaborative Filtering with R and Postgres

# Working Model #
![user_similarity.png](https://bitbucket.org/repo/X8jgjy/images/1726101590-user_similarity.png)

![rating vs count.png](https://bitbucket.org/repo/X8jgjy/images/2566489454-rating%20vs%20count.png)

![competency_name vs count.png](https://bitbucket.org/repo/X8jgjy/images/4049470048-competency_name%20vs%20count.png)



Dashboard of the Shiny Server application :


![dashboard_page.png](https://bitbucket.org/repo/X8jgjy/images/4230105991-dashboard_page.png)


Recommendation Engine using User Based Collaborative Filtering :

![Recommendation Engine in working.png](https://bitbucket.org/repo/X8jgjy/images/832845844-Recommendation%20Engine%20in%20working.png)